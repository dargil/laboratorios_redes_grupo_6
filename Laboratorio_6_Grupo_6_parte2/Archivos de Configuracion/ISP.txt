configure terminal
interface ser2/1
clock rate 56000
encapsulation ppp
ip address 192.168.6.2 255.255.255.0
no shutdown
exit
interface gigabitEthernet 1/0
ip address 209.165.201.225 255.255.255.224
no shutdown
exit
router rip
version 2
network 192.168.6.0
network 209.165.201.0
exit
exit
copy running-config startup-config