/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author German Daniel Rojas , Jefferson Marin, Juan Parada
 */
public class ServidorTcp implements Runnable {

    private final int PUERTO = 1030;//variable que contiene el numero del puerto del servidor.
    private DataInputStream lectorMensajes;//permite leer algunas de las peticiones que envie el cliente
    private DataOutputStream salidaMensajes;//permite enviar mensajes y paquetes de salida a un cliente.
    private Socket socketCliente;//sirve como variable para contener y escuchar los clientes que se conecten.
    private ServerSocket socketServer;//sirve para manipular tanto la salida de datos como para administrar las entradas del cliente.

    public ServidorTcp() throws IOException {
        this.socketCliente = new Socket(); //cuando se ejecute el progrmama se inicializara los  objetos de los sockets
        this.socketServer = new ServerSocket(PUERTO);
    }

    private synchronized void InicioServidorT() throws IOException {

        while (true) {//ciclo que al fucionarse con hilos permite la multiple conexion al server.
            try {
                this.socketCliente = this.socketServer.accept();//sirve para detectar cuando un cliente se conecte.
                System.out.println("SE CONECTO UN CLIENTE");
                this.salidaMensajes = new DataOutputStream(socketCliente.getOutputStream());//se inicializan los flujos de entrda y salida
                this.lectorMensajes = new DataInputStream(this.socketCliente.getInputStream());
                //---------------------------------------
                String mensajeCliente = "";
                byte dataClient[] = new byte[256];//arreglo de bytes, para guardar el mensaje que envia el cliente.
                int nBytesLeidos = this.lectorMensajes.read(dataClient);//se lee lo que se recibe del cliente en bytes y se cuentan.
                //-----------------------------------------
                if (nBytesLeidos > 0) {//entra si el mensaje recibido supera los 0 bytes
                    mensajeCliente = new String(dataClient, 0, (nBytesLeidos - 1));//se pasa de bytes a cadena de caracteres.
                    System.out.println("Mensaje del cliente = " + mensajeCliente);//imprimir una cabecera http
                }
                //----------------------------------------------
                String archivo = this.BuscaArchivo(mensajeCliente);
                System.out.println("Archivo fue: " + archivo);//solo para ver que archivo fue encontrado por el server
                byte[] arrBytes1 = this.ConvertidorArchivo(archivo);
                this.salidaMensajes.write(arrBytes1, 0, arrBytes1.length);//se envia el arreglo de bytes del archivo al cliente.
                this.salidaMensajes.flush();//se empuja la totalidad de los datos.
                this.socketCliente.close();//se cierra el socket
                if (this.socketCliente.isClosed()) {
                    this.socketCliente = this.socketServer.accept();//se abrira uno nuevo en caso de que se cierre
                }
            } catch (Exception error) {
                System.out.println(error.getMessage());//en caso de que no funcione notificara el por que?
            }

        }
    }

    private byte[] ConvertidorArchivo(String nameFile) throws FileNotFoundException, IOException {
        File archivo = new File(nameFile);//se busca el archivo y se pasa a formato de la clase File
        int nBytes = (int) archivo.length();//se cuenta el numero de bytes que posee el archivo.
        byte[] arrBytes1 = new byte[nBytes];//se crea un arreglo de bytes con el numero anterior obtenido.
        //-----------------------------------------
        FileInputStream converterByte = new FileInputStream(archivo);//se transfiere el archivo a la clase que lo pasara a bytes.
        converterByte.read(arrBytes1);//se pasa el archivo a bytes y se guarda en el arreglo de bytes,creado al principio.
        converterByte.close();//se cierra la conversion del archivo pulidamente.
        return arrBytes1;//se retorna un arreglo de bytes lleno
    }

    public String BuscaArchivo(String mensajeCliente) {
        String[] arrFiles = {"AR1.html", "AR2.html", "AR3.html", "404.html", "/ "};//los archivos en el deposito bd
        int i = 0;
        while (true) {//busca por nombres comparandolo con el numero de bytes y con el contenido
            if (mensajeCliente.indexOf(arrFiles[i]) > 0) {
                if (i == 4) {
                    return "AR.html";
                }
                return arrFiles[i];    // retorna solo el nombre del archivo o del directorio del archivo 
            } else {
                if (i == 4) {
                    return arrFiles[3];
                }
                i++;
            }
        }
    }

    public void HiloServidor() {
        new Thread(ServidorTcp.this).start();//es modo de implementar hilos multiples en java sin tener que utlizar el main
    }

    @Override
    public void run() {// es el cotenido del hilo que va ejecutar
        try {
            this.InicioServidorT();
        } catch (IOException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }

}
