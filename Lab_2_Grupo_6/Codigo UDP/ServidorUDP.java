// Librerias necesarias para la realizacion del servidor UDP

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Random;

/**
 *
 * @author jefferson Marin, Juan Parada, German Rojas
 * @since 14/02/2018
 */
public class ServidorUDP { //clase que me permitira ejecutar el servidor

    public static void main(String[] args) throws SocketException, IOException {
        DatagramSocket servidorSocket = new DatagramSocket(9876);//maneja sockets para enviar y recibir datagramas UDP el cual funciona como servidor, a este se le especifica un puerto si este esta ocupado envia una excepcion
        byte[] datoRecibido = new byte[1024]; //arreglo de bytes, para guardar el mensaje que envia el cliente.
        byte[] datoEnviado = new byte[1024]; //arreglo de bytes, para guardar el mensaje que envia el servidor.al cliente
        Random aleatorio = new Random(); //objeto que me permite generar numeros aleatorios
        try {
            System.out.println("--Servidor UDP iniciado correctamente -->"); //mensaje por consola que indica que el servidor se ha iniciado
            while (true) { //ciclo infinito que permite que el servidor se mantenga escuchando
                DatagramPacket paqueteRecibido = new DatagramPacket(datoRecibido, datoRecibido.length); //crear instancias de datagramas que van a ser recibidos por el servidor los cuales son enviados por el cliente
                servidorSocket.receive(paqueteRecibido); //se recibe el paquete enviado desde el cliente
                int numero = aleatorio.nextInt(10); //se genera un numero aleatorio del 1 al (10)  
                if (numero < 5) { //si el numero aleatorio es menor que 5
                    String dato = new String(paqueteRecibido.getData()); //se obtienen los datos enviados por el cliente y se le agsinan a la variable dato
                    System.out.println("--> RECIBIDO: " + dato);//se muestra en consola los datos recibidos 
                    InetAddress IP = paqueteRecibido.getAddress(); //se obtiene la direccion IP¨del cliente
                    int puerto = paqueteRecibido.getPort(); //se obtiene el puerto del cliente
                    if (dato.startsWith("ping")) { //si el el mesaje enviado por el cliente es un ping 
                        datoEnviado = dato.getBytes(); //la ping ingresado se castea a bytes y se le asigna al dato de envio
                        DatagramPacket sendPacket = new DatagramPacket(datoEnviado, datoEnviado.length, IP, puerto);//crear instancias de datagramas que van a ser enviados al cliente
                        servidorSocket.send(sendPacket); //se envia los datagramas de respuesta al cliente en este caso es el espejo
                    } else { //si el dato enviado por el cliente es una palabra
                        byte[] datoEnviadop = new byte[1024]; //arreglo de bytes, para guardar el mensaje que envia el servidor.al cliente
                        String nuevoDato = dato.toUpperCase();//se convierte la palabra enviada por el cliente a mayusculas
                        datoEnviadop = nuevoDato.getBytes(); ////la palabra en mayusculas se castea a bytes y se le asigna al dato de envio
                        DatagramPacket sendPacket = new DatagramPacket(datoEnviadop, datoEnviadop.length, IP, puerto);//crear instancias de datagramas que van a ser enviados al cliente
                        servidorSocket.send(sendPacket); //se envia los datagramas de respuesta al cliente
                    }

                } else {
                    servidorSocket.disconnect(); //se desconecta DatagramSocket de los paquetes enviados por el cliente
                    System.out.println("--> El paquete se perdió"); //se muestra mensaje en consola diciendo que los paquetes se perdieron lo que indica que el numero aleatorio generado es mayor a 5
                }
            }
        } catch (Exception e) {//escuchador de excepciones
            System.out.println("Error: " + e.getMessage());//se muestra en pantalla el error
            servidorSocket.close(); //si existe algun error se cierra el DatagramSocket del servidor
            System.exit(0); //se cierra la ejecucion del programa con un tiempo de espera de o segundos
        }
    }
}
