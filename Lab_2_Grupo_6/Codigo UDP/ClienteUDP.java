
// Librerias necesarias para la realizacion del cliente UDP
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 *
 * @author jefferson Marin, Juan Parada, German Rojas
 * @since 14/02/2018
 */
public class ClienteUDP { //clase que me permitira ejecutar el cliente

    public static void main(String[] args) throws SocketException, UnknownHostException, IOException {
        BufferedReader leerDatos = new BufferedReader(new InputStreamReader(System.in)); //este objeto me permite leer datos por consola
        DatagramSocket clienteSocket = new DatagramSocket(); //maneja sockets para enviar y recibir datagramas UDP el cual funciona como cliente
        InetAddress IPAddress = InetAddress.getByName("localhost"); //objeto que almacena la ip local
        System.out.println("---Cliente UDP iniciado Correctamente-->");
        byte[] datoEnvio = new byte[1024]; //arreglo de bytes, para guardar el mensaje que enviara el cliente.
        byte[] datoRecibido = new byte[1024]; //arreglo de bytes, para guardar el mensaje que envia el servidor.
        try {
            String palabra = leerDatos.readLine(); //se lee la palabra que el cliente desea obtener en mayusculas
            long TInicio, TFin, tiempo; //variables de tipo long para gestionar el tiempo 
            TInicio = System.currentTimeMillis(); //se toma el tiempo de inicio de la ejecucion del cliente para enviar una palabra
            datoEnvio = palabra.getBytes(); //la palabra ingresada se castea a bytes y se le asigna al dato de envio
            DatagramPacket sendPacket = new DatagramPacket(datoEnvio, datoEnvio.length, IPAddress, 9876);//crear instancias de datagramas que van a ser enviados
            clienteSocket.send(sendPacket);//se envia los datagramas 
            DatagramPacket receivePacket = new DatagramPacket(datoRecibido, datoRecibido.length); //crear instancias de datagramas que van a ser recibidos
            clienteSocket.setSoTimeout(1000);//tiempo de espera al servidor de 1 segundo
            try {
                clienteSocket.receive(receivePacket); //se recibe el paquete enviado desde el servidor
                String nuevaPalabra = new String(receivePacket.getData());//se obtiene los datos enviados por el servidor y se le asignan a una variable String (nuevaPalabra)
                TFin = System.currentTimeMillis();//se toma el tiempo final de la ejecucion del programa
                tiempo = TFin - TInicio;//tiempo total del envio y recepcion de los datos
                //  tiempo = tiempo / (long) 1000;
                System.out.println("->>" + nuevaPalabra + " " + tiempo + " ms");
            } catch (Exception e) {//escuchador de excepciones
                System.out.println("-->>Solicitud expirada"); //si el tiempo es mayor a 1 segundo la solicitud expira
            }

            for (int i = 0; i < 10; i++) { //ciclo para enviar los 10 pings
                byte[] datoEnviop = new byte[1024]; //arreglo de bytes, para guardar el mensaje que envia el cliente.
                long TInicio2, TFin2, tiempo2;
                TInicio2 = System.currentTimeMillis();//variables de tipo long para gestionar el tiempo 
                String h = "ping " + (i + 1); //variable String que permite almacenar el ping + su numero correspondiente
                System.out.println("Send>: " + h); // se muestra el ping que se enviara al servidor 
                datoEnviop = h.getBytes(); //la palabra ingresada se castea a bytes y se le asigna al dato de envio
                DatagramPacket sendPacketPing = new DatagramPacket(datoEnviop, datoEnviop.length, IPAddress, 9876); //crear instancias de datagramas que van a ser enviados especificando su ip puerto y el dato a enviar
                clienteSocket.send(sendPacketPing);//se envian los datagramas
                DatagramPacket receivePacketPing = new DatagramPacket(datoRecibido, datoRecibido.length);//crear instancias de datagramas que van a ser recibidos
                clienteSocket.setSoTimeout(1000);//tiempo de espera al servidor de 1 segundo
                try {
                    clienteSocket.receive(receivePacket); //se recibe el paquete enviado desde el servidor
                    String nuevaPalabra = new String(receivePacket.getData()); //se obtiene los datos enviados por el servidor y se le asignan a una variable String (nuevaPalabra)
                    TFin2 = System.currentTimeMillis();//se toma el tiempo final de la ejecucion del programa
                    tiempo2 = TFin2 - TInicio2; //tiempo total del envio y recepcion de los datos
                    // tiempo2 = tiempo2 / (long) 1000;
                    System.out.println("->>" + nuevaPalabra + " " + tiempo2 + " ms");
                } catch (Exception e) { //escuchador de excepciones
                    System.out.println("Solicitud expirada");  //si el tiempo es mayor a 1 segundo la solicitud expira
                }
            }

        } catch (Exception e) {//escuchador de excepciones
            System.out.println("Excepcion: " + e.getMessage()); //se muestra en pantalla el error
            clienteSocket.close(); //si existe algun error se cierra el DatagramSocket del cliente
            System.exit(0); //se cierra la ejecucion del programa con un tiempo de espera de o segundos
        }
    }
}
